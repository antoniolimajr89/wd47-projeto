import { getCartoesSalvos } from "../server/sync.js";
import { IDBSubscribeCartoes } from "../storage/db.js";
import { notificar } from "./notificacao.js";

const mural = document.querySelector('.mural');
const template = document.querySelector('#template-cartao');
let numeroCartao = 0;

IDBSubscribeCartoes(exibirCartoes);

export async function exibirCartoes(cartoesLocais) {
    let listaCartoes = [];

    try {
        listaCartoes = await getCartoesSalvos();
    }
    catch(e) {
        listaCartoes = cartoesLocais;
        if (!listaCartoes.length) {
            notificar('Não há cartões salvos localmente para serem exibidos!');
        }
    }

    mural.innerHTML = '';
    listaCartoes.forEach(cartao => {
        adicionarCartao(cartao.conteudo, cartao.cor);
    });
}

export function adicionarCartao(conteudo, cor = '')
{
    numeroCartao++;
    const cartao = template.content.firstElementChild.cloneNode(true);
    cartao.innerHTML = cartao.innerHTML.replaceAll('{{NUMERO_CARTAO}}', numeroCartao).replace('{{CONTEUDO_CARTAO}}', conteudo);
    cartao.style.backgroundColor = cor;
    mural.append(cartao);
}

export function toggleLayout() {
    mural.classList.toggle('mural--linha');
}

export function getCartoes()
{
    const cartoes = mural.querySelectorAll('.cartao');
    return Array.from(cartoes).map(cartao => {
        return {
            conteudo: cartao.querySelector('.cartao-conteudo').textContent.trim(),
            cor: cartao.style.backgroundColor
        }
    });
}

// exclusão do cartão
mural.addEventListener('click', function(event) {
    if (event.target.classList.contains('opcoesDoCartao-remove')) {
        const cartao = event.target.closest('.cartao');
        cartao.classList.add('cartao--some');
        cartao.addEventListener('transitionend', () => cartao.remove());
    }
});

// mudança da cor do cartão
mural.addEventListener('change', function(event) {
    if (event.target.type === 'radio') {
        const cartao = event.target.closest('.cartao');
        cartao.style.backgroundColor = event.target.value;
    }
});

// mudança de cor do cartão via teclado
mural.addEventListener('keypress', function(event) {
    let isLabel = event.target.tagName === 'LABEL';
    if (isLabel && (event.key === 'Enter' || event.key === ' ')) {
        event.target.click();
    }
});